#ifndef _XBOOK_FS_H
#define	_XBOOK_FS_H

#include <stddef.h>
#include <stdint.h>
#include <types.h>
#include <sys/stat.h>
#include <xbook/spinlock.h>
#include <xbook/fsal.h>
#include <xbook/memspace.h>
#include <xbook/list.h>

...

/* 缓存空间 */
struct address_space{
    list_t clean_nodes;      /* 不需要页面缓存同步的干净节点链表 */
    list_t dirty_nodes;      /* 需要页面缓存同步的脏节点链表 */
    list_t locked_nodes;     /* 在内存中被锁住的页面链表 */
    unsigned long nr_nodes;  /* 在地址空间中正被使用且常驻内存的页面数 */
    struct address_space_operations * a_ops; /* 操纵文件系统的操作 */
    struct inode * host;     /* 文件的索引节点 */
    struct mem_space_t * i_mmspace; /* 使用address_space的私有映射链表 */
    struct mem_space_t * i_mmspace_shared; /* 使用该地址空间中共享映射的mem_space链表 */
    spinlock_t i_shared_lock;         /* 保护此结构的自旋锁 */
};
...
#endif /* _XBOOK_FS_H */
