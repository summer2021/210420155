
#ifndef _XBOOK_PAGEMAP_H
#define _XBOOK_PAGEMAP_H

#include<arch/mempool.h>
#include<xbook/fs.h>
#include<xbook/spinlock.h>


extern atomic_t node_cache_size;
extern unsigned int node_hash_bits;

extern mem_node_t **node_hash_table;

#define NODE_HASH_BITS (node_hash_bits)
#define NODE_HASH_SIZE (1 << NODE_HASH_BITS)
#define node_hash(mapping,index) (node_hash_table+_node_hashfn(mapping,index))


extern spinlock_cacheline_t nodecache_lock_cacheline;
extern spinlock_cacheline_t pagemap_lru_lock_cacheline;

extern void page_cache_init(unsigned long);

static inline unsigned long _node_hashfn(struct address_space * mapping, unsigned long index)
{
#define i (((unsigned long) mapping)/(sizeof(struct inode) & ~ (sizeof(struct inode) - 1)))
#define s(x) ((x)+((x)>>NODE_HASH_BITS))
	return s(i+index) & (NODE_HASH_SIZE-1);
#undef i
#undef s
}

/******************************** 获取/释放页面高速缓存的页面 **************************/
#define node_cache_get(x) get_node(x)

#define node_cache_release(x) __free_node(x)

/******************************** 搜索页面高速缓存 ***********************************/

extern mem_node_t * __find_get_node(struct address_space *, unsigned long, mem_node_t **);

#define find_get_node(mapping, index) __find_get_node(mapping, index, node_hash(mapping, index))

extern mem_node_t * __find_lock_node(struct address_space *, unsigned long, mem_node_t **);

#define find_lock_node(mapping, index) __find_lock_node(mapping, index, node_hash(mapping, index))

#endif  // _XBOOK_PAGEMAP_H