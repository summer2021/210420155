#ifndef _XBOOK_SWAP_H
#define _XBOOK_SWAP_H

#define SWAP_CLUSTER_MAX 32

#include <xbook/pagemap.h>

extern spinlock_cacheline_t nodecache_lock_cacheline;
#define node_cache_lock (nodecache_lock_cacheline.lock)

extern spinlock_cacheline_t nodemap_lru_lock_cacheline;
#define node_map_lru_lock nodemap_lru_lock_cacheline.lock

extern int nr_active_pages;
extern int nr_inactive_pages;

extern void activate_node(mem_node_t *);

#endif 