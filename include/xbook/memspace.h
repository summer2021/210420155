#ifndef _XBOOK_MEMSPACE_H
#define _XBOOK_MEMSPACE_H

...

typedef struct mem_space {
    unsigned long start;        /* 空间开始地址 */
    unsigned long end;          /* 空间结束地址 */
    unsigned long page_prot;    /* 空间保护 */
    unsigned long flags;        /* 空间的标志 */
    vmm_t *vmm;                 /* 空间对应的虚拟内存管理 */
    struct mem_space *next;     /* 所有空间构成单向链表 */
    /*增加部分*/
    unsigned long vm_pgoff;     /* 在已被映射文件里对齐的页面的偏移*/
    struct file * vm_file;      /* 指向被映射的文件的指针 */
} mem_space_t;

...
#endif /* _XBOOK_MEMSPACE_H */
