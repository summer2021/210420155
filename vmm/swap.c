

#include "stddef.h"
#include <arch/mempool.h>
#include <xbook/list.h>
#include <xbook/memspace.h>
#include <xbook/spinlock.h>
#include <xbook/pagemap.h>
#include <xbook/swap.h>
#include <xbook/task.h>
#include <xbook/schedule.h>

/********************************* 在LRU链表添加页面 ****************************/
void lru_cache_add(mem_node_t * node)
{
    if(!NodeLRU(node)){
        spin_lock(&node_map_lru_lock);
        if(!TestSetNodeLRU(node))
            add_node_to_inactive_list(node);
        spin_unlock(&node_map_lru_lock);
    }
}

void add_node_to_active_list(mem_node_t * node)
{
    do{
        SetNodeActive(node);
        list_add(&(node)->lru, &active_list);
        nr_active_nodes++;
    }while(0);
}

void add_node_to_inactive_list(mem_node_t * node)
{
    do{
        SetNodeActive(node);
        list_add(&(node)->lru, &inactive_list);
        nr_active_nodes++;
    }while(0);
}



/********************************* 在删除链表添加页面 ****************************/
void lru_cache_del(mem_node_t * node)
{
    spin_lock(&node_map_lru_lock);
    if(TestClearNodeLRU(node)){
        if(NodeActive(node)){
            del_node_from_active_list(node);
        }else{
            del_node_from_inactive_list(node);
        }
    }
    spin_unlock(&node_map_lru_lock);
}

void del_node_from_active_list(mem_node_t * node)
{
    do{
        list_del(&(node)->lru);
        ClearNodeActive(node);
        nr_active_nodes--;
    }while(0);
}

void del_node_from_inactive_list(mem_node_t * node)
{
    do{
        list_del(&(node)->lru);
        nr_inactive_nodes--;
    }while(0);
}

void active_node(mem_node_t *node)
{
    spin_lock(&node_map_lru_lock);
    active_node_nolock(node);
    spin_unlock(&node_map_lru_lock);
}

static inline void active_node_nolock(mem_node_t * node)
{
    if(NodeLRU(node) && !NodeActive(node)){
        del_node_from_inactive_list(node);
        add_node_to_active_list(node);
    }
}


void active_node(mem_node_t * node)
{
    spin_lock(&node_map_lru_lock);
    active_node_nolock(node);
    spin_unlock(&node_map_lru_lock);
}

static inline void activate_node_nolock(mem_node_t * node)
{
    if(NodeLRU(node) && !NodeActive(node)){
        del_node_from_inactive_list(node);
        add_node_to_active_list(node);
    }
}

static inline void activate_page_nolock(mem_node_t * node)
{
	if (NodeLRU(node) && !NodeActive(node)) {
		del_node_from_inactive_list(node);
		add_node_to_active_list(node);
	}
}

void activate_node(mem_node_t * node)
{
	spin_lock(&node_map_lru_lock);
	activate_page_nolock(node);
	spin_unlock(&node_map_lru_lock);
}
