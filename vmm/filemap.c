#include "stddef.h"
#include <arch/mempool.h>
#include <xbook/spinlock.h>
#include <xbook/list.h>
#include <xbook/memspace.h>
#include <xbook/spinlock.h>
#include <xbook/pagemap.h>
#include <xbook/fs.h>
#include <xbook/swap.h>
#include <xbook/schedule.h>
#include <xbook/debug.h>

spinlock_cacheline_t nodecache_lock_cacheline = {SPIN_LOCK_INIT_UNLOCKED()};
spinlock_cacheline_t nodemap_lru_lock_cacheline = {SPIN_LOCK_INIT_UNLOCKED()};



typedef struct { volatile int counter; } atomic_t;

#define ATOMIC_INIT(i)	{ (i) }


atomic_t node_cache_size = ATOMIC_INIT(0);
unsigned int node_hash_bits;

/**
  * 高速缓存表
  * 存放四种页面
  * 1. 读内存映像文件时所产生的页面碎片
  * 2. 从块设备中读出的块，或从文件系统中读出的块
  * 3. 匿名页面
  * 4. 共享内存去页面
  */ 
mem_node_t **node_hash_table;

/*********************************** 从高速缓存添加页面 **********************************************/

/**
 * 添加节点进缓存
 *
 */
void add_to_node_cache(mem_node_t * node, struct address_space * mapping, unsigned long offset)
{
    spin_lock(&node_cache_lock);
    __add_to_node_cache(node, mapping, offset, node_hash(mapping, offset));
    spin_unlock(&node_cache_lock);
    lru_cache_add(node);
}

/**
 * 判断页面是否锁了,有问题
 *
 */
int add_to_node_cache_unique(mem_node_t * node, struct address_space * mapping, unsigned long offset, struct page **hash)
{
    int err;
    mem_node_t *alias;

    spin_lock(&node_cache_lock);
    alias = __find_page_nolock(mapping, offset, *hash);

    err = 1;
    if(!alias){
        __add_to_node_cache(node, mapping, offset, hash);
        err = 0;
    }
    
    spin_unlock(&node_cache_lock);
    if(!err)
    lru_cache_add(node);
    return err;
}


static inline void __add_to_node_cache(mem_node_t * node, struct address_space * mapping, unsigned long offset, struct page **hash)
{
    unsigned long flags;
    // 清除页面所有标志
    flags = node->flags & ~(1 << NODE_uptodate | 1 << NODE_error | 1 << NODE_dirty | 
                            1 << NODE_referenced | 1 << NODE_arch_1 | 1 << NODE_checked); 
    node->flags = flags | (1 << NODE_locked);
    node_cache_get(node);
    node->index = offset;
    add_node_to_inode_queue(mapping, node);
    add_node_to_hash_queue(node, hash);
}


static inline void add_node_to_inode_queue(struct address_space * mapping, mem_node_t * node)
{
    list_t * head = &mapping->clean_nodes;

    mapping->nr_nodes++;
    list_add(&node->list, head);
    node->mapping = mapping;
}

static void add_node_to_hash_queue(mem_node_t * node, mem_node_t ** p)
{
    mem_node_t * next = *p;

    *p = node;
    node->next_hash = next;
    node->pprev_hash = &node->next_hash;
    atmoic_inc(&node_cache_size);
}

static inline mem_node_t * __find_page_nolock(struct address_space *mapping, unsigned long offset, mem_node_t *node)
{
	goto inside;

	for (;;) {
		node = node->next_hash;
inside:
		if (!node)
			goto not_found;
		if (node->mapping != mapping)
			continue;
		if (node->index == offset)
			break;
	}

not_found:
	return node;
}

/*********************************** 从高速缓存删除页面 **********************************************/

void remove_inode_node(mem_node_t *node)
{
    if(!NodeLocked(node))
        return;
    
    spin_lock(&node_cache_lock);
    __remove_inode_node(node);
    spin_unlock(&node_cache_lock);
}

void __remove_inode_node(mem_node_t * node)
{
    remove_node_from_inode_queue(node);
    remove_node_from_hash_queue(node);
}

static inline void remove_node_from_inode_queue(mem_node_t * node)
{
    struct address_space * mapping = node->mapping;

    if(mapping->a_ops->removepage)
        mapping->a_ops->removepage(node);
    list_del(&node->list);
    node->mapping = NULL;
    wmb();
    mapping->nr_nodes--;
}

static inline void remove_node_from_hash_queue(mem_node_t * node)
{
    mem_node_t * next = node->next_hash;
    mem_node_t ** pprev = node->pprev_hash;

    if(next)
        next->pprev_hash = pprev;
    *pprev = next;
    node->pprev_hash = NULL;
    atomic_dec(&node_cache_size);
}


/*****************************************搜索高速缓存****************************************/
mem_node_t * __find_get_node(struct address_space * mapping, unsigned long offset, mem_node_t **hash)
{
    mem_node_t * node;

    spin_lock(&node_cache_lock);
    node = __find_page_nolock(mapping, offset, * hash);
    if(node)
        node_cache_get(node);
    spin_unlock(&node_cache_lock);
    return node;
}

static inline mem_node_t * __find_node_nolock(struct address_space * mapping, unsigned long offset, mem_node_t *node)
{
    goto inside;

    for(;;){
        node = node->next_hash;
        inside:
        if(!node)
            goto not_found;
        if(node->mapping != mapping)
            continue;
        /* 如果address和index都匹配，则返回 */
        if(node->index == offset)
            break;
    }

    not_found:
    return node;
}

mem_node_t * __find_lock_node(struct address_space * mapping, unsigned long offset, mem_node_t **hash)
{
    mem_node_t * node;

    spin_lock(&node_cache_lock);
    node = __find_lock_node_helper(mapping, offset, * hash);
    spin_unlock(&node_cache_lock);
    return node;
}

void lock_node(mem_node_t *node)
{
	if (TryLockNode(node))
		__lock_node(node);
}

static void __lock_page(struct page *page)
{
	// 哈希，找到页面对应zone->wait_table哪个队列
	wait_queue_head_t *waitqueue = page_waitqueue(page);
	// 为进程初始化等待队列
	struct task_struct *tsk = task_current;
	DECLARE_WAITQUEUE(wait, tsk);
	// 将进程加入等待队列中
	add_wait_queue_exclusive(waitqueue, &wait);
	// 睡眠直到获得锁
	for (;;) {
		// 设为不可中断睡眠
		set_task_state(tsk, TASK_UNINTERRUPTIBLE);
		// 如果还是睡眠，调用sync_page()函数调度页面同步后援存储器
		if (PageLocked(page)) {
			sync_page(page);
			schedule();
		}
		// 尝试锁住页面
		if (!TryLockPage(page))
			break;
	}
	// 获得了锁
	__set_task_state(tsk, TASK_RUNNING);
	remove_wait_queue(waitqueue, &wait);
}


void unlock_page(mem_node_t *node)
{
	// 找到等待队列
	wait_queue_head_t *waitqueue = page_waitqueue(node);
	// 清洗位
	ClearPageLaunder(node);
	// 这是一个必须在进行多处理器可见的位操作前必须调用的内存块操作
	smp_mb__before_clear_bit();
	// 清除NODE_locked位
	if (!test_and_clear_bit(NODE_locked, &(node)->flags))
		BUG();
	// 完成smp内存块操作
	smp_mb__after_clear_bit(); 
	// 如果有进程在等待队列中，则唤醒他们
	if (waitqueue_active(waitqueue))
		wake_up_all(waitqueue);
}

static mem_node_t * __find_lock_node_helper(struct address_space *mapping, unsigned long offset, mem_node_t *hash)
{
    mem_node_t * node;

    repeat:
    node = __find_node_nolock(mapping, offset, hash);
    if(node){
        node_cache_get(node);
        if(TryLockNode(node)){
            spin_unlock(&node_cache_lock);
            lock_node(node);
            spin_lock(&node_cache_lock);
            if(node->mapping != mapping || node->index != offset){
                UnlockNode(node);
                node_cache_release(node);
                goto repeat;
            }
        }
    }
    return node;
}

/**
 * 节点高速缓存初始化
 * mem_nodes: 系统中的物理节点数
 */
void node_cache_init(unsigned long mem_nodes)
{
    unsigned long htable_size, order;
    htable_size = mem_nodes;
    htable_size *= sizeof(mem_node_t *);
    /* 计算高速缓存哈希表大小，分配2*order个页 */
    for(order = 0; (PAGE_SIZE << order) < htable_size; order++)
        ;
    do{
        unsigned long tmp = (PAGE_SIZE << order) / sizeof(mem_node_t *);
        node_hash_bits = 0;
        while((tmp >>= 1UL) != 0UL)
            node_hash_bits++;

        node_hash_table = (mem_node_t **)mem_node_alloc_pages(order, MEM_NODE_TYPE_NORMAL);
    }while(node_hash_table == NULL && --order > 0);

    emeprint("Page-cache hash table entries: %d (order: %ld, %ld bytes)\n",
	       (1 << node_hash_bits), order, (PAGE_SIZE << order));
	if (!node_hash_table)
		errprint("Failed to allocate page hash table\n");
    memset((void *)node_hash_table, 0, NODE_HASH_SIZE * sizeof(mem_node_t *));
}

/**
* 标记页面被引用过
*/
void mark_node_accessed(mem_node_t * node)
{
    if(!NodeActive(node) && NodeReferenced(node)){
        activate_node(node);
        ClearNodeReferenced(node);
    }else{
        SetNodeReferenced(node);
    }
}

