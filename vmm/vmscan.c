
#include <arch/mempool.h>
#include <arch/page.h>
#include <xbook/swap.h>
#include <xbook/pagemap.h>
#include <xbook/file.h>
#include <xbook/schedule.h>
#include <xbook/vmm.h>


static void refill_inactive(int nr_nodes)
{
    list_t * entry;
    
    spin_lock(&node_map_lru_lock);
    entry = active_list.prev;
    while(nr_nodes && entry != &active_list){
        mem_node_t *node;

        node = list_owner(entry, mem_node_t, lru);
        entry = entry->prev;

        if(NodeTestandClearReferenced(node)){
            list_del(&node->lru);
            list_add(&node->lru, &active_list);
            continue;
        }
        nr_nodes--;
        
        del_node_from_active_list(node);
        add_node_to_inactive_list(node);
        SetNodeReferenced(node);
    }
    spin_unlock(&node_map_lru_lock);
}


static int shrink_cache(int nr_nodes, mem_range_t * range, int priority)
{
    list_t * entry;
    int max_scan = nr_inactive_nodes / priority;
    int max_mapped = min((nr_nodes << (10 - priority)),
                          max_scan / 10);
    
    spin_lock(&node_map_lru_lock);
    while(--max_scan >= 0 && 
        (entry = inactive_list.prev) != &inactive_list){
            mem_node_t * node;
            task_t * current = task_current;
            if(unlikely(current->ticks == 0)){
                spin_unlock(&node_map_lru_lock);
                current->state = TASK_RUNNING;
                schedule();
                spin_lock(&node_map_lru_lock);
                continue;
            }
            node = list_owner(entry, mem_node_t, lru);

            list_del(entry);
            list_add(entry, &inactive_list);

            if(unlikely(!node_count(node)))
                continue;
            
            if(node_range(node) != range)
                continue;
            
            if(!node->buffers && (node_count(node) != 1 || ! node->mapping))
                goto node_mapped;
            
            if(unlikely(TryLockNode(node))){
                if(NodeLaunder(node)){
                    node_cache_get(node);
                    spin_unlock(&node_map_lru_lock);
                    wait_on_node(node);
                    node_cache_release(node);
                    spin_lock(&node_map_lru_lock);
                }
                continue;
            }

            if(NodeDirty(node) && is_node_cache_freeable(node) && node->mapping){
                int (* writenode)(mem_node_t *);
                
                writenode = node->mapping->a_ops->writenode;
                if(writenode){
                    ClearNodeDirty(node);
                    SetNodeLaunder(node);
                    node_cache_get(node);
                    spin_unlock(&node_map_lru_lock);

                    writenode(node);
                    node_cache_release(node);

                    spin_lock(&node_map_lru_lock);
                    continue;
                }
            }
            if(node->buffers){
                spin_unlock(&node_map_lru_lock);

                node_cache_get(node);

                if(try_to_release_node(node)){
                    if(!node->mapping){
                        spin_lock(&node_map_lru_lock);
                        UnlockNode(node);
                        __lru_cache_del(node);

                        node_cache_release(node);

                        if(--nr_nodes)
                            continue;
                        break;
                    }else{
                        node_cache_release(node);
                        spin_lock(&node_map_lru_lock);
                    }
                }else{
                    UnlockNode(node);
                    node_cache_release(node);

                    spin_lock(&node_map_lru_lock);
                    continue;
                }
            }
            spin_lock(&node_cache_lock);

            if(!node->mapping || ! is_node_cache_freeable(node)){
                spin_unlock(&node_cache_lock);
                UnlockNode(node);
                node_mapped:
                if(--max_mapped >= 0)
                    continue;
                
                spin_unlock(&node_map_lru_lock);
                swap_out(priority, range);
                return nr_nodes;
            }
            
            if(NodeDirty(node)){
                spin_unlock(&node_cache_lock);
                UnlockNode(node);
                continue;
            }

            if(likely(!NodeSwapCache(node))){
                __remove_inode_node(node);
                spin_unlock(&node_cache_lock);
            }else{
                swap_entry_t swap;
                swap.val = node->index;
                __delete_from_swap_cache(node);
                spin_unlock(&node_cache_lock);
                swap_free(swap);
            }

            __lru_cache_del(node);
            UnlockNode(node);

            node_cache_release(node);

            if(--nr_nodes)
                continue;
            break;
        }
        spin_unlock(&node_map_lru_lock);
        return nr_nodes;
}

/* 收缩所有高速缓存 */
static int shrink_caches(mem_range_t * range, int priority, int nr_nodes)
{
    int chunk_size = nr_nodes;
    unsigned long ratio;

    nr_nodes -= kmem_cache_reap();
    if(nr_nodes <= 0)
        return 0;
    
    nr_nodes = chunk_size;
    ratio = (unsigned long) nr_nodes * nr_active_nodes / ((nr_inactive_nodes + 1) * 2);
    refill_inactive(ratio);

    nr_nodes = shrink_cache(nr_nodes, range, priority);
    if(nr_nodes <= 0)
        return 0;
    
    shrink_dcache_memory(priority);
    shrink_icache_memory(priority);

    # ifdef CONFIG_QUOTA
    shrink_dqcache_memory(DEF_PRIORITY);
    # endif

    return nr_nodes;
}

int try_to_free_nodes()
{
    mem_range_t * range;
    zonelist_t * zonelist;
    unsigned long pf_free_nodes;
    int error = 0;

    pf_free_nodes = current->flags & PF_FREE_NODES;
    current->flags &= ~PF_FREE_NODES;

    for_each_pgdat(pgdat){
        zonelist = pgdat->node_zonelists + ( GFP_ZONEMASK);
        error |= try_to_free_range(zonelist->zones[0]);
    }

    current->flags |= pf_free_nodes;
    return error;
}

int try_to_free_nodes_range(mem_range_t * range)
{
    int priority = DEF_PRIORITY;
    int nr_nodes = SWAP_CLUSTER_MAX;

    do{
        nr_nodes = shrink_caches(range, priority, nr_nodes);
        if(nr_nodes <= 0)
            return 1;
    }while(--priority);

    out_of_memor();
    return 0;
}


/**************** 换出进程页面 *********************/
struct vmm_t *swap_mm = &init_mm;

static int swap_out(unsigned int priority, mem_range_t * range)
{
    int counter, nr_nodes = SWAP_CLUSTER_MAX;
    vmm_t *mm;

    counter = mmlist_nr;

    do{
         task_t * current = task_current;
        if(unlikely(current->ticks == 0)){
            current->state = TASK_RUNNING);
            schedule();
        }

        spin_lock(&mmlist_lock);
        mm = swap_mm;
        while(mm->swap_address == TASK_SIZE || mm = &init_mm){
            mm->swap_address = 0;
            mm = list_owner(mm->mm_list.next, vmm_t, mm_list);
            if(mm == swap_mm)
                goto empty;
            swap_mm = mm;
        }

        atmoic_inc(&mm->mm_users);
        spin_unlock(&mmlist_lock);

        nr_nodes = swap_out_mm(mm, nr_nodes, &counter, range);

        mmput(mm);

        if(!nr_nodes)
            return 1;
    } while(--counter >= 0);

    return 0;

    empty:
        spin_unlock(&mmlist_lock);
        return 0;
}

static inline int swap_out_vmm(vmm_t * mm, int count,
                              int * mmcounter, mem_range_t * range)
{
    unsigned long address;
    mem_space_t * vma;

    spin_lock(&mm->node_table_lock);
    address = mm->swap_address;
    if(address == TASK_SIZE || swap_mm != mm){
        ++ * mmcounter;
        goto out_unlock;
    }
    vma = find_vma(mm, address);
    if(vma){
        if(address < vma->start)
            address = vma->start;

        for(;;){
            count = swap_out_vma(mm, vma, address, count, range);
            vma = vma->next;
            if(!vma)
                break;
            if(!count)
                goto out_unlock;
            address = vma->start;
        }
    }

    mm->swap_address = TASK_SIZE;

    out_unlock:
        spin_lock(&mm->node_table_lock);
    return count;
}

static inline int swap_out_vma(vmm_t * mm, mem_space_t * vma, unsigned long address, int count, mem_range_t * range)
{
    pde_t * pgdir;
    unsigned long end;

    if(vma->flags & VM_RESERVED)
        return count;

    pgdir = pgd_offset(mm, address);

    end = vma->end;

    do{
        count = swap_out_pte(mm, vma, pgdir, address, end, count, range);
        if(!count)
            break;
        address = (address + PGDIR_SIZE) & PGDIR_MASK;
        pgdir++;
    }while(address && (address < end));
    return count;
}


static inline int swap_out_pte(vmm_t * mm, mem_space_t * vma, pde_t * dir, 
                               unsigned long address, unsigned long end, int count, mem_range_t * range)
{
    pte_t * pte;
    unsigned long pmd_end;

    if(pmd_one(*dir))
        return count;
    if(pmd_bad(*dir)){
        pmd_ERROR(* dir);
        pmd_clear(dir);
        return count;
    }

    pte = pte_offset(dir, address);

    pmd_end = (address + PMD_SIZE) & PMD_MASK;
    if(end > pmd_end)
        end = pmd_end;
    
    do{
        if(pte_present(* pte)){
            mem_node_t * node = pte_node(*pte);

            if(VALID_NODE(node) && ! NodeReserved(node)){
                count -= try_to_swap_out(mm, vma, address, pte, node, range);
                if(!count){
                    address += NODE_SIZE;
                    break;
                }
            }
        }
        address += NODE_SIZE;
        pte++;
    } while(address && (address < end));

    mm->swap_address = address;
    return count;
}


static inline int try_to_swap_out(vmm_t * mm, mem_space_t * vma, 
                                  unsigned long address, pte_t * node_table, mem_node_t * node, mem_range_t * range)
{
    pte_t pte;
    swap_entry_t entry;

    if((vma->flags & VM_LOCKED) || ptep_test_and_clear_young(node_table)){
        mark_node_accessed(node);
        return 0;
    }

    if(NodeActive(node))
        return 0;
    
    if(! memclass(node_zone(node), range))
        return 0;
    
    if(TryLockNode(node))
        return 0;
    
    flush_cache_node(vma, address);
    pte = ptep_get_and_clear(node_table);
    flush_tlb_node(vma, address);

    if(pte_dirty(pte))
        set_node_dirty(node);

    if(NodeSwapCache(node)){
        entry.val = node->index;
        swap_duplicate(entry);
        set_swap_pte:
            set_pte(node_table, swp_entry_to_pte(entry));
        drop_pte:
            mm->rss--;
        UnlockNode(node);
        {
            int freeable = node_count(node)- !! node->buffers <= 2;
            node_cache_release(node);
            return freeable;
        }
    }
    if(node->mapping)
        goto drop_pte;
    if(!NODE_dirty(node))
        goto drop_pte;
    if(node->buffers)
        goto preserve;
    

    for(;;){
        entry = get_swap_node();
        if(!entry.val)
            break;
        
        if(add_to_swap_cache(node, entry) == 0){
            SetNodeUptodate(node);
            set_node_dirty(node);
            goto set_swap_pte;
        }
        swap_free(entry);
    }

    preserve:
        set_pte(node_table, pte);
        UnlockNode(node);
        return 0;
}


/************ 页面交换守护程序 ***************/

static int kswapd_init(void){
    print("Starting kswapd\n");
    swap_setup();
    kernel_thread(kswapd, NULL, CLONE_FS | CLONE_FILES | CLONE_SIGNAL);
    return 0;
}

int kswapd(void * unused)
{
    struct task_struct * tsk = task_current;
    DECLARE_WAITQUEUE(wait, tsk);

    daemonize();
    strcpy(tsk->comm, "kswapd");
    sigfillset(&tsk->blocked);

    tsk->flags |= PF_MEMALLOC;

    for(;;){
        __set_current_state(TASK_INTERRUPTIBLE);
        add_wait_queue(&kswapd_wait, &wait);

        mb();
        if(kswapd_can_sleep())
            schedule();

        __set_current_state(TASK_RUNNING);
        remove_wait_queue(&kswapd_wait, &wait);

        kswapd_balance();
        run_task_queue(&tq_disk);
    }
}


static int kswapd_can_sleep()
{
    mem_range_t * range;
    int i;

    for(i = 0; i < MEM_RANGE_NR; ++i){
        range = &mem_ranges[i];
        if(! range->need_balance)
            continue;
        return 0;
    }
    return 1;
}


static int kswapd_balance()
{
    int need_more_balance = 0, i;
    mem_range_t * range;

    for(i = 0; i < MEM_RANGE_NR; ++i){
        range = &mem_ranges[i];
        task_t * current = task_current;
        if(unlikely(current->ticks == 0))
            shedule();
        if(!range->need_balance)
            continue;
        if(!try_to_free_nodes_range(range)){
            range->need_balance = 0;
            __set_current_state(TASK_INTERUPTIBLE);
            schedule_timeout(HZ);
            continue;
        }
        if(check_range_need_balance(range))
            need_more_balance = 1;
        else
            range->need_balance = 0;
    }
    return need_more_balance;
}
