#ifndef _X86_MEMPOOL_H
#define _X86_MEMPOOL_H
/* 物理内存，一个节点可能是一个页，也可能是两个N个 */
typedef struct _mem_node {
    unsigned int count;
    unsigned int flags;         
    int reference; 
    list_t list;
    mem_section_t *section;
    mem_cache_t *cache;
    mem_group_t *group;
    mem_range_t *range;
    unsigned long offset;
    unsigned long index;
    list_t lru;
    struct address_space * mapping;
    struct _mem_node * next_hash;
    struct _mem_node ** pprev_hash;  
    struct buffer_head * buffers;

} mem_node_t;

/* 内存区域 */
typedef struct {
    unsigned int start;
    unsigned int end;
    size_t pages;
    mem_section_t sections[MEM_SECTION_MAX_NR];
    int need_balance;
    mem_node_t *node_table;
} mem_range_t;


static inline mem_range_t *node_range(mem_node_t *node)
{
	return node->range;
}

/* 节点的状态 */
/* 页面的状态 */
// 在页面必须在磁盘I/O所在内存时，该位被设置（I/O启动，被设置，结束，被释放）
#define NODE_locked		 0	/* Page is locked. Don't touch. */
// 硬盘I/O发生错误
#define NODE_error		 1
// 当页面被映射并且被映射的索引哈希表引用时，被设置。
// 在LRU链表上移动页面做页面替换时使用该位。
#define NODE_referenced		 2
// 当一个页面从磁盘无错误地读出时，该位被设置
#define NODE_uptodate		 3
// 显示页面是否需要被刷新到磁盘中去。保证一个脏页面在写出之前不会被释放掉
#define NODE_dirty		 4
#define NODE_unused		 5
// 当页面存在active_list或inactive_list中时被设置
#define NODE_lru			 6
// 当前位于LRU active_list链表上，并在页面清除时取消该位，是否处于活跃态
#define NODE_active		 7

// 高端页面位
#define NODE_highmem		11
#define NODE_checked		12	/* kill me in 2.5.<early>. */
#define NODE_arch_1		13
// 该位标志了禁止换出的页面，该位在系统初始化时，由引导内存分配器设置，
// 接下来用于标志空页面或者不存在的页面
#define NODE_reserved		14
// 对页面替换策略标志，在VM要换出页面时，该位被设置，并调用writepage函数
// 如果遇到了该位与PG_locked位同时被设置的页面，需要等待I/O完成
#define NODE_launder		15	/* written out by VM pressure.. */
#define NODE_fs_1			16	/* Filesystem specific */

#define NodeLocked(node)	test_bit(NODE_locked, &(node)->flags)
#define ClearPageUptodate(node)	clear_bit(NODE_uptodate, &(node)->flags)
#define NodeDirty(node)		test_bit(NODE_dirty, &(node)->flags)
#define SetNodeDirty(node)	set_bit(NODE_dirty, &(node)->flags)
#define ClearNodeDirty(node)	clear_bit(NODE_dirty, &(node)->flags)
#define NodeLocked(node)	test_bit(NODE_locked, &(node)->flags)
#define LockNode(node)		set_bit(NODE_locked, &(node)->flags)
#define TryLockNode(node)	test_and_set_bit(NODE_locked, &(node)->flags)
#define NodeChecked(node)	test_bit(NODE_checked, &(node)->flags)
#define SetNodeChecked(node)	set_bit(NODE_checked, &(node)->flags)
#define NodeLaunder(node)	test_bit(NODE_launder, &(node)->flags)
#define SetNodeLaunder(node)	set_bit(NODE_launder, &(node)->flags)
#define ClearNodeLaunder(node)	clear_bit(NODE_launder, &(node)->flags)


#define NodeError(node)		test_bit(NODE_error, &(node)->flags)
#define SetNodeError(node)	set_bit(NODE_error, &(node)->flags)
#define ClearNodeError(node)	clear_bit(NODE_error, &(node)->flags)
#define NodeReferenced(node)	test_bit(NODE_referenced, &(node)->flags)
#define SetNodeReferenced(node)	set_bit(NODE_referenced, &(node)->flags)
#define ClearNodeReferenced(node)	clear_bit(NODE_referenced, &(node)->flags)
#define NodeTestandClearReferenced(node)	test_and_clear_bit(NODE_referenced, &(node)->flags)
#define NodeReserved(node)	test_bit(NODE_reserved, &(node)->flags)


#define NodeActive(node)	test_bit(NODE_active, &(node)->flags)
#define SetNodeActive(node)	set_bit(NODE_active, &(node)->flags)
#define ClearNodeActive(node)	clear_bit(NODE_active, &(node)->flags)

#define NodeLRU(node)		test_bit(NODE_lru, &(node)->flags)
#define TestSetNodeLRU(node)	test_and_set_bit(NODE_lru, &(node)->flags)
#define TestClearNodeLRU(node)	test_and_clear_bit(NODE_lru, &(node)->flags)


#endif /*_X86_MEMPOOL_H */